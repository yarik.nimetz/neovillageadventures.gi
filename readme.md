# Neo-village adventures!

It's 2015, you have to live a slice of life
a simple schoolboy from an ordinary village somewhere in the west of Ukraine.
In the game, I drew the simplest carefree day of my 9 years old
therefore, with all the features of walking down the street, and most importantly,
I risked partially transferring the atmosphere those times. 
But, unfortunately, I could not tolerate "war activism"
on the walls in a abandoned buildings, next to which they usually lay
drunkards with weed, abandoned buildings across the street
the main square of the village, monuments to Soviet heroes on the street
Heroes of the Heavenly Hundred and so on, so it's only a semi-simulator
of that "wonderful" era.(The game in the Ukrainian language)

Location with a house:

![firstlockation](screen1.png)

Location with a backyard: 

![secondlocation](screen2.png)

[Play game](https://yarikn.itch.io/neo-village-adventures)

## License 
```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                           November 2024
 
Copyright (C) 2024 Yaroslav Nimets <yarik.nimetz@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

You just DO WHAT THE FUCK YOU WANT TO.
```
